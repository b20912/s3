/*Mini Activity*/
// Initialize express api running at port 4000

const express = require('express');
const app = express();
const PORT = 4000;
app.use(express.json());

let users = [
	{
		name: "John",
		age: 18,
		username: "johnsmith99"
	},
	{
		name: "Johnson",
		age: 21,
		username: "johnson1991"
	},
	{
		name: "Smith",
		age: 19,
		username: "smithMike12"
	}
];

let products = [
	{
		name: "Shampoo",
		price: 299,
		isActive: true
	},
	{
		name: "Soap",
		price: 99,
		isActive: true
	},
	{
		name: "Conditioner",
		price: 399,
		isActive: true
	}
];

app.get('/users', (req,res) => {
	return res.send(users);
});

app.get('/products', (req,res) => {
	return res.send(products);
});

app.post('/users', (req,res) => {
	//hasOwnProperty() returns a boolean if the property name passed exists or not exists in the given object
	if(!req.body.hasOwnProperty("name")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter NAME"
		})
	}

	if(!req.body.hasOwnProperty("age")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter AGE"
		})
	}

	if(!req.body.hasOwnProperty("username")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter USERNAME"
		})
	}

})

app.listen(PORT, () =>{
	console.log("Running on port " + PORT);
});